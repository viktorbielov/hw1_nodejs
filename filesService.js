const fs = require("fs");
const path = require("path");

function createFile(req, res, next) {
  let files = fs.readdirSync("./files/");
  const { filename, content, password} = req.body;

  if (files.includes(filename)) {
    res.status(400).json({message: `${filename} already exists`});
  }

  if (!filename) {
    res.status(400).json({message: 'Please specify "filename" parameter'});
  }

  if (!content) {
    res.status(400).json({message: 'Please specify "content" parameter'});
  }

  const fileExtension = path.extname(filename).split('.')[1];

  if (["log", "txt", "json", "yaml", "xml", "js"].includes(fileExtension)) {
    fs.writeFileSync(`./files/${filename}`, content);

    if (password) {
      let passwords = {};
  
      if (fs.existsSync("./passwords.json")) {
        passwords = JSON.parse(
          fs.readFileSync("./passwords.json", { encoding: "utf8", flag: "r" })
        );
      }
  
      passwords = JSON.stringify({
        ...passwords,
        [filename]: password,
      });
  
      fs.writeFileSync("./passwords.json", passwords);
    }
    res.status(200).send({ message: "File created successfully" });
  }
  else{
    res.status(400).json({
      message: `File extension allowed: log, txt, json, yaml, xml, js`
    });
  }
}

function getFiles(req, res, next) {
  let files = fs.readdirSync("./files/");

  res.status(200).send({
    message: "Success",
    files: files,
  });
}

const getFile = (req, res, next) => {
  const files = fs.readdirSync("./files/");
  const fileName = req.params.filename;

  if (fs.existsSync("./passwords.json")) {
    const passwords = JSON.parse(fs.readFileSync("./passwords.json", { encoding: "utf8", flag: "r" })
    );

    if (fs.existsSync("passwords.json") && passwords.hasOwnProperty(fileName)) {
      if (!req.query.hasOwnProperty("password")) {
        res.status(400).json({message: `File ${fileName} is protected by password`});
      }

      const password = req.query.password;

      if (passwords[fileName] !== password) {
        res.status(400).json({message: `Password for ${fileName} is invalid`});
      }
    }
  }

  if (!files.includes(fileName)) {
    res.status(400).json({message: `No file with ${fileName} filename found`});
  }

  const fileMeta = fs.fstatSync(fs.openSync(`./files/${fileName}`));

  const content = fs.readFileSync(`./files/${fileName}`, {
    encoding: "utf8",
    flag: "r",
  });

  const extension = path.extname(fileName).split(".").join("");

  res.status(200).send({
    message: "Success",
    filename: fileName,
    content: content,
    extension: extension,
    uploadedDate: fileMeta.birthtime,
  });
}

function editFile(req, res, next) {
  const files = fs.readdirSync("./files/");

  const fileName = req.params.filename;

  if (fs.existsSync("./passwords.json")) {
    const passwords = JSON.parse(
      fs.readFileSync("./passwords.json", { encoding: "utf8", flag: "r" })
    );

    if (passwords.hasOwnProperty(fileName)) {
      if (!req.query.hasOwnProperty("password")) {
        res.status(400).json({message: `File ${fileName} is protected by password`});
      }

      const password = req.query.password;

      if (passwords[fileName] !== password) {
        res.status(400).json({message: `Password for ${fileName} is invalid`});
      }
    }
  }

  if (!files.includes(fileName)) {
    res.status(400).send({ message: "File doesn't exist" });
    return;
  }

  if (!req.body.content) {
    res.status(400).send({ message: "Please specify content parameter" });
    return;
  }

  fs.writeFileSync(`./files/${fileName}`, req.body.content, { flag: "a" });

  res.status(200).send({ message: "File updated successfully" });
}

function deleteFile(req, res, next) {
  const files = fs.readdirSync("./files/");

  const fileName = req.params.filename;

  if (!files.includes(fileName)) {
    next({ status: 400, message: "File doesn't exist" });
  }

  if (fs.existsSync("passwords.json")) {
    let passwords = JSON.parse(
      fs.readFileSync("./passwords.json", { encoding: "utf8", flag: "r" })
    );

    if (passwords.hasOwnProperty(fileName)) {
      if (!req.query.hasOwnProperty("password")) {
        next({
          status: 400,
          message: `File ${fileName} is password protected`,
        });
      }

      const password = req.query.password;

      if (passwords[fileName] !== password) {
        next({
          status: 400,
          message: `Password for file ${fileName} is invalid`,
        });
      }
    }

    delete passwords[fileName];

    fs.writeFileSync("./passwords.json", JSON.stringify(passwords));
  }

  fs.rmSync(`./files/${fileName}`);

  res.status(200).send({ message: "File deleted successfully" });
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile,
};
