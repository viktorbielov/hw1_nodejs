const fs = require('fs');
const path = require('path');
const morgan = require('morgan')
const express = require('express');
const app = express();
const { filesRouter } = require('./filesRouter.js');
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'logs.log'), { flags: 'a' });

app.use(express.json());

app.use(morgan('combined', { stream: accessLogStream }));

app.use('/api/files', filesRouter);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
}
start();

//ERROR HANDLER
app.use(errorHandler)

function errorHandler (err, req, res, next) {
  if (err.status === 400 || err.status === 404) {
    res.status(err.status).send({'message': err.message});
  }
  res.status(500).send({'message': 'Server error'});
}
